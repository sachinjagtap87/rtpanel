$(document).ready(function(){
	$('.navbar-header').on('click', '.navbar-toggle', function(){
		$('.navbar-collapse').toggleClass('in');
	});

	$('.hero-carousel').owlCarousel({
		items: 1,
	    loop: false,
	    margin: 0
	});

	$('.video-slider').owlCarousel({
		items: 5,
	    loop: false,
	    margin: 5,
	   	nav: true,
	    navText: ['<i class="fa fa-chevron-left">','<i class="fa fa-chevron-right">'],
	    dots: false,
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:3
	        },
	        1000:{
	            items:5
	        }
	    }
	});

	$('.video-slider').on('click', '.video-link', function(){
	    var videoId = $(this).data('rel');
	    if ($(window).width() <= 768) {
	      var url = 'https://www.youtube.com/watch?v='+videoId;
	      window.open(url, '_blank');
	    }else{
	      $('.video-container').html('<iframe width="100%" height="100%" src="https://www.youtube.com/embed/'+ videoId +'?autoplay=1" frameborder="0" allowfullscreen></iframe>');
	      $('#video-popup').fadeIn(200);
	      $('body').append('<div class="modal-backdrop fade in"></div>')
	    }
  	});
   	// close video
	$('#popup-bg, .close-popup').click(function(event) {
	    $('.video-container').empty();
	    $('.modal-backdrop, #video-popup').fadeOut(200);
	});
	

	$('.news-carousel').owlCarousel({
		items: 1,
	    loop: false,
	    margin: 0,
	    nav: true,
	    dots: false,
	    navText: ['<i class="fa fa-chevron-left">','<i class="fa fa-chevron-right">'],
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:2
	        },
	        1000:{
	            items:1
	        }
	    }
	});

	$('#our-partners-slider').owlCarousel({
		items: 5,
	    loop: false,
	    margin: 70,
	    nav: true,
	    navText: ['<i class="fa fa-chevron-left">','<i class="fa fa-chevron-right">'],
	    dots: false,
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:3
	        },
	        1000:{
	            items:5
	        }
	    }
	});

});


function startTime() {
    var today = new Date();
	var dayArr = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
	var monthArr = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    document.getElementById('day-name').innerHTML = dayArr[today.getDay()];
    document.getElementById('date').innerHTML = today.getDate();
	document.getElementById("month-name").innerHTML = monthArr[today.getMonth()];
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    m = checkTime(m);
    s = checkTime(s);
    document.getElementById('time').innerHTML =
    h + ":" + m + ":" + s;
    var t = setTimeout(startTime, 500);
}
function checkTime(i) {
    if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
}
// $(window).load(function() {
// 	startTime()
// });
